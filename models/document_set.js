const mongoose = require('mongoose')

const DocumentSetSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    documentIds: {
      type: String,
      required: true,
    }
  }
)

mongoose.model('DocumentSet', DocumentSetSchema)
