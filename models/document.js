const mongoose = require('mongoose')

const DocumentSchema = new mongoose.Schema(
  {
    createdAt: {
      type: Date,
      required: true,
    },
    updatedAt: {
      type: Date,
      required: true,
    },
    filepath: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    fileId: {
      type: String,
      required: true,
    }
  }
)

mongoose.model('Document', DocumentSchema)
