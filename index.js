const express = require('express')
const mongoose = require('mongoose')

require('./models/document')
require('./models/document_set')

const app = express()

app.use(express.json())
app.use((_, res, next) => {
  res.set('Access-Control-Allow-Origin', '*')
  res.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
  res.set('Access-Control-Allow-Headers', 'Origin, Content-Type, Access-Control-Allow-Origin')
  next()
})
// app.use(compression({ threshold: 0 }))

mongoose.connect(process.env.DB_URI || 'mongodb://mongo:27017', {
  connectTimeoutMS: 5000,
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
})
  .catch((err) => {
    console.log(err)
    console.log('Error connecting to database')
    process.exit(1)
  })

require('./routes/document')(app)

const server = app.listen(4000, (err) => {
  if (err) {
    console.log(err, 'Error starting server')
    process.exit(1)
  }
  console.log('Listening on port 4000')
})

process.on('SIGTERM', () => {
  shutdown()
})

process.on('SIGINT', () => {
  shutdown()
})

async function shutdown() {
  try {
    console.log('shutting down')
    await new Promise(r => server.close(r))
    await mongoose.connection.close()
    // Process should exit automatically when event loop is clear
  } catch (err) {
    console.log('Unclean shutdown, encountered error')
    console.log(err)
    process.exit(1)
  }
}
