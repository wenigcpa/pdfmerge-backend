## pdfmerge backend

Built with Express, mongoose. Expects a mongo connection. Running with docker-compose will create a locally networked mongo instance.

## Development

To start a test server: `docker-compose up --build`

## Production

To run the backend on port 4000 use the following:

`docker-compose up -d`

PDF files will be stored in `./data`.

Database files will be stored in `./db`.

Make sure both of these exist and are directories.