const mongoose = require('mongoose')
const Document = mongoose.model('Document')
const DocumentSet = mongoose.model('DocumentSet')
const uuid = require('uuid')
const multer = require('multer')
const upload = multer()
const fs = require('fs')
const pdf = require('pdfjs')
const _ = require('lodash')

module.exports = (app) => {
  app.get('/documents', loadDocuments)
  app.post('/documents', upload.array('document'), uploadDocument)
  app.put('/documents', updateDocument)
  app.delete('/documents', deleteDocument)
  app.get('/documents/combine', combineDocuments)
  app.put('/documentsets', createDocumentSet)
  app.get('/documentsets', loadDocumentSets)
  app.delete('/documentsets', deleteDocumentSet)
  app.get('/documents/:id', viewDocument)
}

async function viewDocument(req, res) {
  const { id } = req.params
  const doc = await Document.findOne({
    _id: mongoose.Types.ObjectId(id),
  })
  const data = await new Promise((rs, rj) => fs.readFile(doc.filepath, (err, _data) => {
    if (err) rj(err)
    else rs(_data)
  }))
  res.set('Content-Type', 'application/pdf')
  res.send(data)
  res.end()
}

async function deleteDocumentSet(req, res) {
  const { _id } = req.body
  const doc = await DocumentSet.findOne({
    _id: mongoose.Types.ObjectId(_id),
  }).lean().exec()
  if (!doc) {
    res.status(404).json({ message: 'Unable to find document set'})
    return
  }
  await DocumentSet.deleteOne({
    _id: mongoose.Types.ObjectId(_id),
  })
  res.status(204)
  res.end()
}

async function loadDocumentSets(req, res) {
  const data = await DocumentSet.find({}).lean().exec()
  res.json(data)
}

async function createDocumentSet(req, res) {
  const { _id, name, documentIds } = req.body
  if (_id) {
    // update
    await DocumentSet.updateOne({
      _id: mongoose.Types.ObjectId(_id),
    }, {
      name,
      documentIds,
    })
    const doc = await DocumentSet
      .findOne({ _id: mongoose.Types.ObjectId(_id) })
      .lean()
      .exec()
    res.json(doc)
  } else {
    const { _doc } = await DocumentSet.create({
      name,
      documentIds,
    })
    res.json(_doc)
  }
}

async function combineDocuments(req, res) {
  const { ids } = req.query
  const _ids = ids.split(',').filter((id) => !!id)
  if (!_ids.length) {
    res.status(400).json({ message: 'No documents selected' })
    return
  }
  const docs = await Document.find({
    _id: {
      $in: _ids.map((id) => mongoose.Types.ObjectId(id)),
    }
  }).lean().exec()
  if (docs.length !== _ids.length) {
    res.status(500).json({ message: 'Document length mismatch' })
    return
  }
  const docsById = _.keyBy(docs, '_id')
  const pdfs = await Promise.all(docs.map((doc) => {
    return new Promise((rs, rj) => {
      fs.readFile(doc.filepath, (err, data) => {
        docsById[doc._id].pdf = data
        if (err) rj(err)
        else rs(data)
      })
    })
  }))
  try {
    const doc = new pdf.Document()
    for (const _id of _ids) {
      const _pdf = docsById[_id].pdf
      const ext = new pdf.ExternalDocument(_pdf)
      doc.addPagesOf(ext)
    }
    const data = await doc.asBuffer()
    res.set('Content-Type', 'application/pdf')
    res.send(data)
    res.end()
  } catch (err) {
    res.status(500).json({ message: err.toString() })
  }
}

async function loadDocuments(req, res) {
  const docs = await Document.find({}).lean().exec()
  res.json(docs)
}

async function uploadDocument(req, res) {
  const { files } = req
  if (!files.length) {
    res.status(400)
    res.json({ message: 'No files attached' })
    return
  }
  const created = []
  for (const file of files) {
    if (file.mimetype !== 'application/pdf') {
      continue
    }
    const id = uuid.v4()
    const filepath = `/data/${id}`
    await new Promise((rs, rj) => fs.writeFile(filepath, file.buffer, (err) => {
      if (err) rj(err)
      else rs()
    }))
    const { _doc } = await Document.create({
      name: file.originalname,
      fileId: id,
      filepath,
      createdAt: new Date(),
      updatedAt: new Date(),
    })
    created.push(_doc)
  }
  res.json(created)
}

async function updateDocument(req, res) {
  const { _id, name } = req.body
  await Document.updateOne({
    _id: mongoose.Types.ObjectId(_id),
  }, {
    name,
    updatedAt: new Date(),
  }).exec()
  const doc = await Document.findOne({
    _id: mongoose.Types.ObjectId(_id),
  }).lean().exec()
  res.json(doc)
}

async function deleteDocument(req, res) {
  const { _id } = req.body
  const doc = await Document.findOne({
    _id: mongoose.Types.ObjectId(_id),
  }).lean().exec()
  if (!doc) {
    res.status(404).json({ message: 'Unable to find document '})
    return
  }
  await new Promise((rs, rj) => fs.unlink(doc.filepath, (err) => {
    if (err) rj(err)
    else rs()
  }))
  await Document.deleteOne({
    _id: mongoose.Types.ObjectId(_id),
  })
  res.status(204)
  res.end()
}
